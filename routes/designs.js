const express = require ('express')
const db = require ('../conf/db')

const route = express.Router()

route.post("/add_designs",(req,res)=>{
    const obI = JSON.stringify(req.body.obI)
    const obT = JSON.stringify(req.body.obT)
    const info = JSON.stringify(req.body.info)
    let sql="INSERT INTO aladin_designs(obI,info,designer,obT) VALUES ?";
    var value=[[obI,info,req.body.designer,obT]];
    
    db.query(sql, [value] ,(err,result)=>{
        if(err){
            return res.status(400).json({
                status: false,
                error:err
            })
        }else{
            return res.status(201).json({
                status: true,
                message:"You successfully created an design !!",
                data: result
            })
        }
    })

    
})

route.get("/designs",(req,res)=>{
    let sql="SELECT * FROM aladin_designs";
    db.query(sql,(err,result)=>{
        if(err){
            return res.status(400).json({
                status: false,
                error: err
            })
        }else{
            return res.status(200).json({
                status:true,
                message: "You successfully retrieve the designs !!",
                data : result
            })
        }
    })
})

module.exports = route