const expres = require("express")
const route = expres()
const db = require("../conf/db")

    route.get("/cloths/items/:id",(req,res)=>{
        let limit = 3
        let id = req.params.id
        let page = req.query.page

        let sql= `SELECT * FROM aldin_models WHERE category=${id} ORDER BY obj LIMIT ${limit}`;

        db.query(sql,(err,result)=>{
            if(err){
                return res.status(400).json({
                    error:err
                })
            }else{
                return res.status(200).json({
                    num_page: page,
                    data: result
                   
                })
                
            }
            
        })
        
        
    })

    route.get("/packs/items/:id",(req,res)=>{
        let limit = 3
        let id = req.params.id
        let page = req.query.page

        let sql= `SELECT * FROM aldin_models WHERE category=${id} ORDER BY obj LIMIT ${limit}`;

        db.query(sql,(err,result)=>{
            if(err){
                return res.status(400).json({
                    error:err
                })
            }else{
                return res.status(200).json({
                    num_page: page,
                    data: result
                   
                })
                
            }
            
        })
        
        
    })

    route.get("/displays/items/:id",(req,res)=>{
        let limit = 3
        let id = req.params.id
        let page = req.query.page

        let sql= `SELECT * FROM aldin_models WHERE category=${id} ORDER BY obj LIMIT ${limit}`;

        db.query(sql,(err,result)=>{
            if(err){
                return res.status(400).json({
                    error:err
                })
            }else{
                return res.status(200).json({
                    num_page: page,
                    data: result
                   
                })
                
            }
            
        })
        
        
    })

    route.get("/gadgets/items/:id",(req,res)=>{
        let limit = 3
        let id = req.params.id
        let page = req.query.page

        let sql= `SELECT * FROM aldin_models WHERE category=${id} ORDER BY obj LIMIT ${limit}`;

        db.query(sql,(err,result)=>{
            if(err){
                return res.status(400).json({
                    error:err
                })
            }else{
                return res.status(200).json({
                    num_page: page,
                    data: result
                   
                })
                
            }
            
        })
        
        
    })

    route.get("/printed/items/:id",(req,res)=>{
        let limit = 3
        let id = req.params.id
        let page = req.query.page

        let sql= `SELECT * FROM aldin_models WHERE category=${id} ORDER BY obj LIMIT ${limit}`;

        db.query(sql,(err,result)=>{
            if(err){
                return res.status(400).json({
                    error:err
                })
            }else{
                return res.status(200).json({
                    num_page: page,
                    data: result
                   
                })
                
            }
            
        })
        
        
    })


module.exports = route