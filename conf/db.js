const mysql = require('mysql');


const db= mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"",
    database:"test_product",
	  multipleStatements: true,
});


db.connect(function(err) {
    if (err){
      console.log(err);
      process.exit(1)
    }
    console.log("Connecté à la base de données MySQL!");
});

module.exports=db;
